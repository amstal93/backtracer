# Governance

due to the actual size of the project, each contributors has the right to vote since his first contribution has been merged in the main branch.

Votes can be launch by creating a issue with label '[***VOTE***](https://gitlab.com/backtracer/backtracer/-/issues/new?issue[title]=%5BVOTE%5D%20&issue[issue_type]=issue) ' in the main project ***Backtracer/Backtracer***

if a vote want to be launch for a technical aspect concerning only one of the submodules (don't forget the potential impact in the other one), the VOTE issue can be create in the concerned submodule  [API-BACKTRACER](https://gitlab.com/backtracer/API-backtracer/-/issues/new?issue[title]=%5BVOTE%5D%20&issue[issue_type]=issue) or [Front-BACKTRACER](https://gitlab.com/backtracer/Front-backtracer/-/issues/new?issue[title]=%5BVOTE%5D%20&issue[issue_type]=issue)

The voting period must be of at least 7 days and you can vote using 👍 and 👎 emotes. 
